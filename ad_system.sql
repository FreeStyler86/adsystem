/*
Navicat MySQL Data Transfer

Source Server         : Ad System
Source Server Version : 50713
Source Host           : 127.0.0.1:33060
Source Database       : ad_system

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2016-08-23 13:19:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ad
-- ----------------------------
DROP TABLE IF EXISTS `ad`;
CREATE TABLE `ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `activation_date` datetime DEFAULT NULL,
  `orientation_id` int(11) DEFAULT NULL,
  `seller_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1: new; 2:ready; 3:active',
  PRIMARY KEY (`id`),
  KEY `ad_orientation_fk` (`orientation_id`),
  CONSTRAINT `ad_orientation_fk` FOREIGN KEY (`orientation_id`) REFERENCES `orientation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ad
-- ----------------------------
INSERT INTO `ad` VALUES ('1', 'PHP fejlesztő', '2016-08-21 17:08:19', '2016-08-23 10:44:14', '2', 'roncz.laszlo@gmail.com', '1');
INSERT INTO `ad` VALUES ('2', 'Titkársági asszisztens', '2016-08-21 17:09:11', '2016-08-22 12:07:59', '1', 'roncz.laszlo@gmail.com', '1');

-- ----------------------------
-- Table structure for orientation
-- ----------------------------
DROP TABLE IF EXISTS `orientation`;
CREATE TABLE `orientation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of orientation
-- ----------------------------
INSERT INTO `orientation` VALUES ('1', 'Asszisztens');
INSERT INTO `orientation` VALUES ('2', 'Informatikus');
INSERT INTO `orientation` VALUES ('3', 'Villamosmérnök');
SET FOREIGN_KEY_CHECKS=1;
