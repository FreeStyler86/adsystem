<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return view('index');
    //For development
    return redirect()->action('AdController@show', ['id' => 1]);
});

Route::resource('ad', 'AdController',['only' => [
    'show'
]]);

Route::post('ad/{id}/activate', [
    'uses' => 'AdController@activate'
]);
