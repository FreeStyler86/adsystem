<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon\Carbon;
use Mail;
use App\Ad;

class AdController extends Controller
{
    /**
     * Display a listing of the ad.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new ad.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created ad in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified ad.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->getAdDetailPage($id);
    }

    /**
     * Show the form for editing the specified ad.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified ad in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Activate the specified ad and send an email to the seller
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        $ad = Ad::find($id);

        if(!$ad){
            abort('404',trans('ad.not_found'));
        }else if($ad->status < 3){
            $ad->status = 3;
            $ad->activation_date = Carbon::now();
            $ad->save();

            $this->sendActivationEmail($ad);
            return $this->getAdDetailPage($ad->id,trans('ad.activation_successful'));
        }

        return $this->getAdDetailPage($ad->id);
    }

    /**
     * Remove the specified ad from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get detail page for the specified ad.
     * It can display the passed message.
     *
     * @param  int  $id
     * @param  string  $message
     * @return \Illuminate\Http\Response
     */
    private function getAdDetailPage($adId,$message='')
    {
        $ad = Ad::find($adId);

        if($ad){
            return view('ad/detail', [
                'positionName' => $ad->position_name,
                'statusText' => $ad->status_text,
                'showActivateButton' => $ad->status < 2 ? true : false,
                'id' => $ad->id,
                'message' => $message
            ]);
        }else{
            abort('404',trans('ad.not_found'));
        }
    }

    /**
     * Notify the seller by an email when the ad is activated
     *
     * @param  Ad  $ad
     * @return boolean
     */
    private function sendActivationEmail($ad)
    {
        Mail::send(
            'emails/adActivation',
            [
                'link' => url('/ad/'.$ad->id)
            ],
            function ($message) use ($ad){
                $message->to($ad->seller_email);
                $message->subject(trans('email.ad_activation_successful_subject'));
            }
        );

        return Mail::failures() ? false : true;
    }
}
