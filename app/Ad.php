<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Orientation;

class Ad extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ad';
    public $timestamps = false;

    /**
     * Get the ad's status text.
     *
     * @param  string  $value
     * @return string
     */
    public function getStatusTextAttribute($value)
    {
        switch ($this->attributes['status']){
            case 1:
                return trans('ad.status_new');
            case 2:
                return trans('ad.status_ready');
            case 3:
                return trans('ad.status_active');
        }
    }

    /**
     * Get the orientation record associated with the ad.
     */
    public function orientation()
    {
        return $this->belongsTo('App\Orientation');
    }
}
