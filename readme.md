# App requirements
- Composer
- PHP >= 5.5.9
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- MySQL

# App configuration
- config/app.php (url)
- config/database.php (mysql)
- config/mail.php (email configuration)
- config/services.php (mailgun email provider configuration)

# Installation
- Check out the repository
- Install environment (Composer, PHP and MySql or use Laravel HomeStead: https://laravel.com/docs/5.2/homestead)
- Run composer install in the application root folder
- Create a database
- Import ad_system.sql into the database
- Configure the application (application url, database, email)
- Run the application (type application url into the browser like http://localhost/adsystem/public).
Application runs from public folder. You should add a virtual host pointing on the folder.