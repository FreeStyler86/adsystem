<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in error messages
    |
    */

    '404' => 'A keresett oldal nem található!',
    '503' => 'A szerver jelenleg nem érhető el!',
];
