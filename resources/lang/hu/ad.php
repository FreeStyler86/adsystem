<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Ad Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the ad class
    |
    */

    'not_found' => 'A keresett hirdetés nem található!',
    'activation_successful' => 'A hirdetést sikeresen aktiválta!',
    'form_title' => 'Hirdetés adatai',
    'name' => 'Hirdetés neve',
    'status' => 'Státusz',
    'activate_button_title' => 'Aktivál',
    'status_new' => 'Friss',
    'status_ready' => 'Kész',
    'status_active' => 'Aktív',
];
