<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Email Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the emails
    |
    */

    'ad_activation_successful_subject' => 'Sikeres aktiválás!',
    'ad_activation_successful_content' => "Kedves ügyfelünk!<br>Az ön hirdetése sikeresen aktiválódott.<br>Hirdetését az alábbi linken tekintheti meg:<br>",
];
