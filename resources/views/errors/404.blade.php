@include('appHeader')
    <div class="httpErrorMessage">
        @if($exception->getMessage())
            {{$exception->getMessage()}}
        @else
            {{ trans('error.404') }}
        @endif
    </div>
@include('appFooter')
