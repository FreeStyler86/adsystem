@include('emails.header')
    {!! trans('email.ad_activation_successful_content') !!}
    <a href={{$link}} target="_blank">{{$link}}</a>
@include('emails.footer')