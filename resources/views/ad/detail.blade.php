@include('appHeader')
    <h1>{{ trans('ad.form_title') }}</h1>
    <form method="POST" action="{{ url('ad/'.$id.'/activate') }}">
        <div class="row"><span>{{ trans('ad.name') }}: </span><span>{{$positionName}}</span></div>
        <div class="row"><span>{{ trans('ad.status') }}: </span><span>{{$statusText}}</span></div>
        {{ csrf_field() }}
        @if ($showActivateButton)
            <button>{{ trans('ad.activate_button_title') }}</button>
        @endif
    </form>
    @if ($message)
        <div class="infoMessage">{{$message}}</div>
    @endif
@include('appFooter')
